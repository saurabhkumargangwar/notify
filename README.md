## Getting Started
You need to have redis installed in your machine. The instructions can be found on their respective websites. Once these are installed follow the below steps to setup the project:
```
$ virtualenv -p python3 newenv
$ source newenv/bin/activate
$ pip3 install -r requirements.txt
```
run migrations
```
python manage.py migrate
```
And start the server with follwoing command
```
$ python manage.py runserver 8000
```
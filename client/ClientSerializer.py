from rest_framework import serializers


class ClientSerializer(serializers.Serializer):
    email = serializers.EmailField()
    name = serializers.CharField(max_length=20)
    password = serializers.CharField(min_length=6, max_length=10)
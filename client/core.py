import random
import redis

r = redis.StrictRedis(host='localhost', port=6379, db=0, decode_responses=True)


def generate_client_id():
	return random.randint(100000, 999999)


def store_client_data(client_id, data):
	r.hmset(client_id, data)


def get_client_data(client_id):
	return r.hgetall(client_id)


def store_client_status(client_id, status):
	key = 'CLIENT:STATUS:'+str(client_id)
	r.set(key, status)


def get_client_status(client_id):
	key = 'CLIENT:STATUS:' + str(client_id)
	return r.get(key)


def delete_client_status(client_id):
	key = 'CLIENT:STATUS:' + str(client_id)
	r.delete(key)
from django.shortcuts import render
from rest_framework import views
from rest_framework.response import Response
from client.ClientSerializer import ClientSerializer
from client.core import store_client_data, get_client_data, generate_client_id, store_client_status, \
	delete_client_status
from notify import settings
from notify.core import disconnect_client

HOST_ADDRESS = getattr(settings, 'HOST')


class Login(views.APIView):
	def post(self, request, *args, **kwargs):
		if not request.data:
			return render(request, 'client/client_login.html', {'error': "Please provide username/password"})

		client_id = request.data['client_id']
		password = request.data['password']

		data = get_client_data(client_id)
		if not data:
			return render(request, 'client/client_login.html', {'error': "Please provide valid client id."})

		is_valid_password = self.verify_password(data, client_id, password)
		if not is_valid_password:
			return render(request, 'client/client_login.html', {'error': "Invalid username/password."})

		host = 'ws://'+HOST_ADDRESS+'/client-' + str(client_id)+'/'

		context = {
			'connection':host,
			'client_id':client_id,
			'client_name': data['name']
		}

		store_client_status(client_id=client_id, status='connected')
		return render(request, 'client/client_connected.html', context)

	def get(self, request):
		return  render(request, 'client/client_login.html')

	def verify_password(self, data, client_id, password):
		return data['client_id'] ==client_id and data['password']==password


class ClientRegistration(views.APIView):

	def post(self, request, *args, **kwargs):
		if not request.data:
			return Response({'Error': "Please provide username/password"}, status="400")

		client_name = request.data.get('name')
		email = request.data.get('email')
		password = request.data.get('password')
		client_id = generate_client_id()
		client_data = {'name':client_name, 'email':email, 'password':password}
		c = ClientSerializer(data=client_data)

		if c.is_valid():
			client_data['client_id'] = client_id
			store_client_data(client_id, client_data)
			return Response(
				{
					'client_id': client_id,
					'message':'clients is registered successfully.'
				},
				status=200,
				content_type="application/json"
			)
		return Response(c.errors, status=200, content_type="application/json")


class Logout(views.APIView):

	def post(self, request, *args, **kwargs):
		client_id = request.data['client_id']
		delete_client_status(client_id=client_id)

		group_name = 'client-'+str(client_id)
		disconnect_client(group_name)
		return render(request, 'client/client_login.html')


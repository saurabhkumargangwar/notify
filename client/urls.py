from django.urls import path

from client import views

urlpatterns = [
    path('register/', views.ClientRegistration.as_view()),
	path('login/', views.Login.as_view()),
	path('logout/', views.Logout.as_view()),
]
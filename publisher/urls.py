from django.urls import path

from publisher import views

urlpatterns = [
    path('message/', views.PublishMessage.as_view())
]
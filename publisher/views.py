from rest_framework import views
from rest_framework.response import Response
from client.core import get_client_status
from notify.core import push_data_to_client


class PublishMessage(views.APIView):
	def post(self, request, *args, **kwargs):
		if not request.data:
			return Response({'Error': "Please provide inputs"}, status="400")

		client_id = request.data.get('client_id')
		if not client_id:
			return Response({'Error': "Please provide client id."}, status="400")

		client_status = get_client_status(client_id)
		if not client_status or client_status!='connected':
			return Response({'Error': "Please provide valid client id."}, status="400")

		message = request.data.get('message')

		push_data_to_client(message, 'client-' + client_id)

		return Response(
			{message:'message published successfully'},
			status=200
		)

import json
from channels.consumer import AsyncConsumer


class NotifyConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        self.room_group_name = self.scope['path'][1:-1]

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.send({
            "type": "websocket.accept"
        })


    async def websocket_receive(self, event): # websocket.receive
        message_data = json.loads(event['text'])
        final_message_data = json.dumps("hi i am saurabh")
        await self.channel_layer.group_send(
			self.room_group_name,
            {
                'type': 'chat_message',
                'message': final_message_data
            }
        )

    async def broadcast_message(self, event):
        await self.send({
            "type": "websocket.send",
            "text": json.dumps(event['message'])
        })

    async def chat_message(self, event):
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                "type": "websocket.send",
                "text": json.dumps({'msg': "Loading data please wait...", 'user': 'admin'})
            })

    async def websocket_disconnect(self, event):
        # when the socket connects
        #print(event)
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
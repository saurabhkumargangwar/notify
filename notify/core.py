from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer


def push_data_to_client(msg, group_name):
	channel_layer = get_channel_layer()
	actual_message = {'msg': msg}
	broadcast_data = {
		'type': 'broadcast_message',
		'message': actual_message
	}
	async_to_sync(channel_layer.group_send)(group_name, broadcast_data)


def disconnect_client(group_name):
	channel_layer = get_channel_layer()
	actual_message = {'msg': 'disconnecting'}
	broadcast_data = {
		'type': 'websocket_disconnect',
		'message': actual_message
	}
	async_to_sync(channel_layer.group_send)(group_name, broadcast_data)


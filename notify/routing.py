'''
Created by shahid on 02/10/18
'''

from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator

from notify.consumer import NotifyConsumer

application = ProtocolTypeRouter({
    # Websocket chat handler
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                [
                    url(r"(?P<client>[a-zA-Z0-9-]+)/$", NotifyConsumer, name='client')
                ]
            )
        ),
    )
})
